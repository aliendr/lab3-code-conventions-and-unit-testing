package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.*;
import org.springframework.jdbc.support.xml.SqlXmlFeatureNotImplementedException;

import java.util.Collections;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doNothing;


class ThreadControllerTests {
    private String mockId = "0";
    private String mockSlug = "mockSlug";
    private Thread thread;
    private List<Post> posts;
    private Vote vote;

    @BeforeEach
    @DisplayName("Set up before testing thread controller")
    void createElements() {
        thread = new Thread("author", Timestamp.valueOf("2020-02-08 12:00:00.0"),
                "forum", "message", mockSlug, "title", 0);
        posts = Collections.emptyList();
        vote = new Vote("nickname", 5);
    }

    @Test
    @DisplayName("CheckIdOrSlug method")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(mockId)))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug))
                    .thenReturn(thread);
            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug(mockSlug));
            assertEquals(thread, controller.CheckIdOrSlug(mockId));

            assertNull(controller.CheckIdOrSlug("99"));
            assertNull(controller.CheckIdOrSlug("sluuuug"));
        }
    }

    @Test
    @DisplayName("CreatePost method - successful")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug))
                        .thenReturn(thread);
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts),
                        controller.createPost(mockSlug, posts));
            }
        }
    }

    @Test
    @DisplayName("Posts method - successful")
    void testPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug))
                    .thenReturn(thread);
            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts),
                    controller.Posts(mockSlug, 5, 1, "tree", true));
        }
    }

    @Test
    @DisplayName("Change method - NOT_FOUND")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(null);
            threadMock.when(() -> ThreadDAO.change(null, thread))
                    .thenThrow(new DataAccessException("Раздел не найден."){});
            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.change(mockSlug, thread));
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."))
                    .getStatusCode(), controller.change(mockSlug, thread).getStatusCode());
        }
    }

    @Test
    @DisplayName("Info method - successful")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);
            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(mockSlug));
        }
    }

    @Test
    @DisplayName("CreateVote method - successful")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("nickname")).thenReturn(user);
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread)
                        .getStatusCode(), controller.createVote(mockSlug, vote).getStatusCode());
            }
        }
    }
}